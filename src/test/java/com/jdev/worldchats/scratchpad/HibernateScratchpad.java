package com.jdev.worldchats.scratchpad;

import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

import com.jdev.worldchats.data.domain.LoginHistory;
import com.jdev.worldchats.data.domain.User;

public class HibernateScratchpad {

	private static final String USER = "root";
	private static final String PASS = "root";
	private static final String URL = "jdbc:mysql://localhost:3306/worldchats";

	enum DB {
		ORACLE, HSQLDB, H2, MYSQL
	}

	public static void main(String args[]) {
		schemaCreate(DB.MYSQL);
	}

	/**
	 * Create SQL for an entity.
	 */
	private static void schemaCreate(DB db) {
		Configuration configuration = new Configuration();

		configuration.addAnnotatedClass(User.class);
		configuration.addAnnotatedClass(LoginHistory.class);

		configuration.setProperty(org.hibernate.cfg.Environment.FORMAT_SQL, "true");
		configuration.setProperty(org.hibernate.cfg.Environment.HBM2DDL_AUTO, "validate");

		switch (db) {
		case ORACLE:
			addOracleConfiguration(configuration);
			break;
		case HSQLDB:
			addHSQLDBConfiguration(configuration);
			break;
		case H2:
			addH2Configuration(configuration);
			break;
		case MYSQL:
			addMYSQLConfiguration(configuration);
			break;
		}

		System.out.println("############## Schema UPDATE to current state ##############");
		SchemaUpdate schemaUpdate = new SchemaUpdate(configuration);
		schemaUpdate.execute(true, false);
		System.out.println("###################################################################");

		System.out.println("\n\n############## Schema CREATE ##############");
		SchemaExport schema = new SchemaExport(configuration);
		schema.create(true, false);
		System.out.println("###########################################");
	}

	private static void addOracleConfiguration(Configuration configuration) {
		configuration.setProperty(Environment.USER, "MACS_COACH_FITNESS").setProperty(Environment.PASS, "macs")
				.setProperty(Environment.URL, "jdbc:oracle:thin:@idevdb.xx-well.local:1521:xxdev").setProperty(Environment.DRIVER, "oracle.jdbc.OracleDriver")
				.setProperty(Environment.DIALECT, "org.hibernate.dialect.Oracle10gDialect");
	}

	private static void addHSQLDBConfiguration(Configuration configuration) {
		configuration.setProperty(Environment.USER, "sa").setProperty(Environment.PASS, "").setProperty(Environment.URL, "jdbc:hsqldb:mem:test")
				.setProperty(Environment.DRIVER, "org.hsqldb.jdbcDriver").setProperty(Environment.DIALECT, "org.hibernate.dialect.HSQLDialect");
	}

	private static void addH2Configuration(Configuration configuration) {
		configuration.setProperty(Environment.URL, "jdbc:h2:mem:test").setProperty(Environment.DRIVER, "org.h2.Driver")
				.setProperty(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");
	}

	private static void addMYSQLConfiguration(Configuration configuration) {
		configuration.setProperty(Environment.USER, USER).setProperty(Environment.PASS, PASS).setProperty(Environment.URL, URL)
				.setProperty(Environment.DRIVER, "com.mysql.jdbc.Driver").setProperty(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
	}
}
