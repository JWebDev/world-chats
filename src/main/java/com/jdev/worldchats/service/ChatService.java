/*
 * Created at 20.05.2015 by fe
 *
 * Copyright Klopotek & Partner GmbH. All Rights Reserved.
 *
 * Version history
 * ===============
 *
 * $Log: $
 */

package com.jdev.worldchats.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.jdev.worldchats.data.domain.ChatMessage;
import com.jdev.worldchats.data.repositoies.RoomRepository;
import com.jdev.worldchats.data.repositoies.UserRepository;

/**
 * @author JDev
 */
@Service
public class ChatService
{

  @Inject
  private UserRepository userRepository;

  @Inject
  private RoomRepository roomRepository;


  public void chat(String roomId, String chatId, ChatMessage message)
  {

  }

}
