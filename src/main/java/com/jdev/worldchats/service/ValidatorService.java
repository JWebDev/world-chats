/**
 * 
 */
package com.jdev.worldchats.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.jdev.worldchats.commons.ErrorMessage;
import com.jdev.worldchats.data.models.ModelObject;

/**
 * @author JDev
 *
 */
@Service
public class ValidatorService {

	@Inject
	private MessageSource messageSource;

	public ModelObject validateModel(ModelObject object) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<ModelObject>> constraintViolations = validator.validate(object);
		if (constraintViolations.size() != 0) {
			object.setErrorMessages(createErrorMessages(constraintViolations));
		}
		return object;
	}

	private List<ErrorMessage> createErrorMessages(Set<ConstraintViolation<ModelObject>> constraintViolations) {
		List<ErrorMessage> errorMessages = new ArrayList<ErrorMessage>();
		for (ConstraintViolation error : constraintViolations) {
			String field = error.getPropertyPath().toString();
			String message = messageSource.getMessage(error.getMessage(), new Object[]{"Unknown error"}, Locale.ENGLISH);
			errorMessages.add(new ErrorMessage(field, message));
		}
		return errorMessages;
	}

}
