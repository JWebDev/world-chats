/*
 * Created at 20.05.2015 by fe
 *
 * Copyright Klopotek & Partner GmbH. All Rights Reserved.
 *
 * Version history
 * ===============
 *
 * $Log: $
 */

package com.jdev.worldchats.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.jdev.worldchats.commons.WorldChatsException;
import com.jdev.worldchats.data.domain.Room;
import com.jdev.worldchats.data.domain.User;
import com.jdev.worldchats.data.enums.RoomType;
import com.jdev.worldchats.data.models.City;
import com.jdev.worldchats.data.models.Country;
import com.jdev.worldchats.data.models.Region;
import com.jdev.worldchats.data.models.RoomModel;
import com.jdev.worldchats.data.repositoies.RoomRepository;
import com.jdev.worldchats.data.repositoies.UserRepository;
import com.jdev.worldchats.data.repositoies.WcRepository;

/**
 * @author JDev
 */
@Service
public class RoomService
{
  @Inject
  private UserRepository userRepository;

  @Inject
  private RoomRepository roomRepository;

  @Inject
  private WcRepository   wcRepository;


  public RoomModel createPrivateRoom(Long userId)
  {
    Room room = createRoom(RoomType.PRIVATE, userId);
    return calculateRoomModelFromEntity(room);
  }


  public RoomModel createPublicRoom(Long userId)
  {
    Room room = createRoom(RoomType.PUBLIC, userId);
    return calculateRoomModelFromEntity(room);
  }


  public RoomModel createCityRoom(Long userId)
  {
    Room room = createRoom(RoomType.CITY, userId);
    return calculateRoomModelFromEntity(room);
  }


  @SuppressWarnings("null")
  public RoomModel connectToRoom(Long roomId, Long userId)
  {

    Room room = roomRepository.findOne(roomId);
    User user = userRepository.findOne(userId);

    if (room == null) {
      new WorldChatsException("The room does not exist");
    }
    room.getUsers().add(user);

    return calculateRoomModelFromEntity(roomRepository.save(room));
  }


  public List<RoomModel> getAllRooms()
  {
    List<RoomModel> roomModels = new ArrayList<RoomModel>();
    List<Room> rooms = roomRepository.findAll();
    for (Room room : rooms) {
      roomModels.add(calculateRoomModelFromEntity(room));
    }
    return roomModels;
  }


  public List<City> getAllCities(String countryCode, String regionCode)
  {
    return wcRepository.getAllCities(countryCode, regionCode);
  }


  public List<Region> getAllRegions(String countryCode)
  {
    return wcRepository.getAllRegions(countryCode);
  }


  public List<Country> getAllCountries()
  {
    return wcRepository.getAllCountries();
  }


  private RoomModel calculateRoomModelFromEntity(Room room)
  {
    RoomModel roomModel = new RoomModel();
    //TODO copy from user to userModel ob correct?
    BeanUtils.copyProperties(roomModel, room);
    return roomModel;
  }


  private Room createRoom(RoomType roomType, Long userId)
  {

    User user = userRepository.findOne(userId);
    if (user == null) {
      //TODO ERROR
    }

    Room room = new Room();
    //TODO create room name
    room.setName("TODO create room name");
    room.setRoomType(roomType);
    Set<User> users = new HashSet<User>();
    users.add(user);
    room.setUsers(users);

    return roomRepository.save(room);
  }
}
