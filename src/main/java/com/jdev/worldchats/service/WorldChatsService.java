/**
 *
 */
package com.jdev.worldchats.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jdev.worldchats.commons.PublicContext;
import com.jdev.worldchats.commons.WorldChatsException;
import com.jdev.worldchats.data.domain.LoginHistory;
import com.jdev.worldchats.data.domain.User;
import com.jdev.worldchats.data.enums.Gender;
import com.jdev.worldchats.data.models.GoModel;
import com.jdev.worldchats.data.models.ModelObject;
import com.jdev.worldchats.data.models.SignUpModel;
import com.jdev.worldchats.data.models.UserModel;
import com.jdev.worldchats.data.repositoies.UserRepository;
import com.jdev.worldchats.data.repositoies.WcRepository;

/**
 * @author JDev
 *
 */
@Service
public class WorldChatsService {

	@Inject
	private UserRepository userRepository;

	/**
	 * Checks if user nickname. If a user is found - is added to the history and
	 * saves. If not, creates and saves a new one.
	 * 
	 * @param goModel
	 * @return UserModel
	 * @throws WorldChatsException
	 */
	public UserModel savePublicUser(GoModel goModel) throws WorldChatsException {

		User user = chekUser(goModel.getNickname(), goModel.getGender(), goModel.getAge());

		if (user != null) {
			user.getLoginHistories().add(new LoginHistory());
		} else {
			user = new User(null, goModel.getNickname(), goModel.getGender(), true, true, goModel.getAge(), PublicContext.getUserIpAddress(), null, null, null,
					null, null, null);
			user.setLoginHistories(new HashSet<LoginHistory>(Arrays.asList(new LoginHistory())));
		}

		return calculateUserModel(userRepository.save(user));
	}

	public UserModel signup(SignUpModel signupModel) throws WorldChatsException {

		User user = chekUser(signupModel.getNickname(), signupModel.getGender(), signupModel.getAge());

		if (user != null) {
			user.getLoginHistories().add(new LoginHistory());
		} else {
			if (userRepository.findByEmail(signupModel.getEmail()) != null) {
				throw new WorldChatsException("The user with this e-mail is already exist.");
			}
			user = new User(null, signupModel.getNickname(), signupModel.getGender(), true, false, signupModel.getAge(), PublicContext.getUserIpAddress(),
					null, signupModel.getEmail(), null, null, null, null);
			// TODO after gray programming - remove? sen password immediatelly
			user.setPassword(StringUtils.newStringUtf8(Base64.decodeBase64(signupModel.getPassword())));
			// TODO - generate Activation key and sent E-mail
			user.setLoginHistories(new HashSet<LoginHistory>(Arrays.asList(new LoginHistory())));
		}

		return calculateUserModel(userRepository.save(user));
	}

	public UserModel signin(String id, String password) throws WorldChatsException {
		User user = userRepository.findByEmailOrNickname(id, id);
		UserModel userModel = null;
		if (user == null) {
			throw new WorldChatsException("The user name or email is wrong");
		}
		// TODO after gray programming - remove
		String passfromClient = StringUtils.newStringUtf8(Base64.decodeBase64(password));
		if (!passfromClient.equals(user.getPassword())) {
			throw new WorldChatsException("The password is wrong");
		}
		userModel = calculateUserModel(user);

		return userModel;
	}

	/**
	 * Checks if user name at the moment is unoccupied, if the private user is
	 * not registered with that name or the user wit that name is not online
	 * 
	 * @param nickname
	 * @param gender
	 * @param age
	 * @return - founded user or WorldChatsException
	 * @throws WorldChatsException
	 */
	private User chekUser(String nickname, Gender gender, int age) throws WorldChatsException {

		User user = userRepository.findByNicknameIsOnlineOrNotPublic(nickname);

		if (user != null) {
			throw new WorldChatsException("The WorldChats user, does now exist, please choiche another nickname.");
		}

		user = userRepository.findByNicknameAndIpAndGenderAndAge(nickname, PublicContext.getUserIpAddress(), gender, age);

		return user;
	}

	public List<User> findAll() {
		return userRepository.findAll();
	}

	private UserModel calculateUserModel(User user) {

		UserModel userModel = new UserModel();
		userModel.setId(user.getId());
		userModel.setNickname(user.getNickname());
		userModel.setGender(user.getGender());
		userModel.setAge(user.getAge());
		userModel.setEmail(user.getEmail());

		return userModel;
	}

}
