package com.jdev.worldchats.tricks;

public class ConsoleStyles {

	// @formatter:off
	public static final String APPLICATION_STARTED = ""
			+ "\n----------------------------------- WORLD CHATS APPLICATION STARTED -----------------------------------\n";
	
	public static final String APPLICATION_DESTROYED = ""
			+ "\n----------------------------------- WORLD CHATS APPLICATION DESTROYED----------------------------------\n";

}
