package com.jdev.worldchats.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jdev.worldchats.tricks.ConsoleStyles;

@WebListener
public class WcContextLoaderListener implements ServletContextListener {

	private static final Logger logger = LoggerFactory.getLogger(WcContextLoaderListener.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		logger.info(ConsoleStyles.APPLICATION_STARTED);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		logger.info(ConsoleStyles.APPLICATION_DESTROYED);
	}

}