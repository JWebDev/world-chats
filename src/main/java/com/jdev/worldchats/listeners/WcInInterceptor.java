/**
 * 
 */
package com.jdev.worldchats.listeners;

/**
 * @author JDev
 *
 */
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.HttpHeaders;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Exchange;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.Conduit;
import org.apache.cxf.ws.addressing.EndpointReferenceType;
import org.springframework.stereotype.Component;

import com.jdev.worldchats.commons.PublicContext;
import com.jdev.worldchats.commons.WorldChatsConstant;

@Component("wcInInterceptor")
public class WcInInterceptor extends AbstractPhaseInterceptor<Message> {
	public WcInInterceptor() {
		super(Phase.RECEIVE);
	}

	@Override
	public void handleMessage(Message message) throws Fault {

		HttpServletRequest request = (HttpServletRequest) message.get("HTTP.REQUEST");
		String sessionId = null;
		HttpSession session = request.getSession(true);

		try {
			if (request.getPathInfo().startsWith("/room")) {
				sessionId = request.getHeader(WorldChatsConstant.SESSION_ID_HTTP_HEADER);
				if (!session.getId().equals(sessionId)) {
					request.getSession().invalidate();
					sendErrorResponse(message, HttpURLConnection.HTTP_UNAUTHORIZED);
					return;
				}
			}
			if (sessionId == null) {
				sessionId = session.getId();
			}
			PublicContext.setSessionId(sessionId);
			PublicContext.setUserIpAddress(getIpAddress(request));
			// fillPublicContext(kasSession, kasSessionId);
		} catch (NotAuthorizedException e) {
			System.out.println("Invalid Session: " + e.getMessage());
			sendErrorResponse(message, HttpURLConnection.HTTP_UNAUTHORIZED);
		} catch (Exception e) {
			System.out.println("Error calling KAS service: " + e.getMessage());
			sendErrorResponse(message, HttpURLConnection.HTTP_INTERNAL_ERROR);
		}
		// TODO set attribute to session
	}

	private void sendErrorResponse(Message message, int responseCode) {
		Message outMessage = getOutMessage(message);
		outMessage.put(Message.RESPONSE_CODE, responseCode);

		// Set the response headers
		@SuppressWarnings("unchecked")
		Map<String, List<String>> responseHeaders = (Map<String, List<String>>) message.get(Message.PROTOCOL_HEADERS);

		if (responseHeaders != null) {
			// responseHeaders.put("WWW-Authenticate", Arrays.asList(new
			// String[]{"Basic realm=realm"}));
			responseHeaders.put("Content-Length", Arrays.asList(new String[] { "0" }));
			List<String> list = responseHeaders.get(Message.CONTENT_TYPE);
			if ((list == null) || (list.size() == 0) || (list.get(0) == null)) {
				responseHeaders.remove(Message.CONTENT_TYPE);
				responseHeaders.put(Message.CONTENT_TYPE, Arrays.asList(new String[] { "application/json" }));
			}
		}
		message.getInterceptorChain().abort();
		try {
			getConduit(message).prepare(outMessage);
			close(outMessage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Message getOutMessage(Message inMessage) {
		Exchange exchange = inMessage.getExchange();
		Message outMessage = exchange.getOutMessage();
		if (outMessage == null) {
			Endpoint endpoint = exchange.get(Endpoint.class);
			outMessage = endpoint.getBinding().createMessage();
			exchange.setOutMessage(outMessage);
		}
		outMessage.putAll(inMessage);
		return outMessage;
	}

	private Conduit getConduit(Message inMessage) throws IOException {
		Exchange exchange = inMessage.getExchange();
		exchange.get(EndpointReferenceType.class);
		Conduit conduit = exchange.getDestination().getBackChannel(inMessage);
		exchange.setConduit(conduit);
		return conduit;
	}

	private void close(Message outMessage) throws IOException {
		OutputStream os = outMessage.getContent(OutputStream.class);
		os.flush();
		os.close();
	}

	private String getIpAddress(HttpServletRequest servletRequest) {

		String ipAddress = servletRequest.getHeader("X-FORWARDED-FOR");
		// String ipAddress = httpServletRequest.getHeader("X-Real-IP");
		if (ipAddress == null) {
			ipAddress = servletRequest.getRemoteAddr();
		}
		return ipAddress;
	}

}
