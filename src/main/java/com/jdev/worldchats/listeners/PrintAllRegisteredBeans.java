/**
 * 
 */
package com.jdev.worldchats.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Listener that outputs to the console all registered beans in the application.
 *
 * @author JDev
 *
 */
@Component
public class PrintAllRegisteredBeans implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger logger = LoggerFactory.getLogger(PrintAllRegisteredBeans.class);

	public void onApplicationEvent(ContextRefreshedEvent event) {
		logger.info("\n\n\n--------------------------------------- ALL REGISTERED BEANS ---------------------------------------");
		for (String bean : event.getApplicationContext().getBeanDefinitionNames()) {
			logger.info(bean);
		}
		logger.info("\n----------------------------------------------------------------------------------------------------\n\n\n");
	}
}
