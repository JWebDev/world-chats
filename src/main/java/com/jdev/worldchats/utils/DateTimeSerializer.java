/*
 * Project: welldoo-tk-service-objective
 *
 * Copyright © 2014 welldoo.de GmbH
 */
package com.jdev.worldchats.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.jdev.worldchats.listeners.WcContextLoaderListener;

/**
 * A Joda {@link DateTime} Serializer for serializing them into an JSON-String.
 * 
 * NOTE: this Serializer is part of "com.fasterxml.jackson2" you need to use the correct Serialization Annotation
 * 
 * @author Andreas de Azevedo
 * 
 */
public class DateTimeSerializer extends JsonSerializer<DateTime> {
	
	private static final Logger logger = LoggerFactory.getLogger(WcContextLoaderListener.class);
	
	private final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	@Override
	public void serialize(DateTime value, JsonGenerator jgen, SerializerProvider provider) throws IOException,
			JsonProcessingException {
		logger.trace("about to serialize a DateTime instance into a readable String now");
		jgen.writeString(formatter.format(value.toDate()));
		
	}

}
