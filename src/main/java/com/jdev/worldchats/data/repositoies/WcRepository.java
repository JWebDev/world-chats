package com.jdev.worldchats.data.repositoies;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.jdev.worldchats.data.models.City;
import com.jdev.worldchats.data.models.Country;
import com.jdev.worldchats.data.models.Region;

/**
 *
 * @author JDev
 */
@Service
public class WcRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Cacheable("cities")
	public List<City> getAllCities(String countryCode, String regionCode) {
		List<City> cities = new ArrayList<City>();

		Query query = entityManager.createNativeQuery(" SELECT id, country, region, name FROM cities where country=\"" + countryCode + "\" and region=\""
				+ regionCode + "\"");
		List<Object[]> regionObjects = query.getResultList();

		for (Object[] object : regionObjects) {
			City city = new City();
			city.setId(((Integer) object[0]).intValue());
			city.setCountry(String.valueOf(object[1]));
			city.setRegion(String.valueOf(object[2]));
			city.setName(String.valueOf(object[3]));
			cities.add(city);
		}

		return cities;
	}

	@Cacheable("regions")
	public List<Region> getAllRegions(String countryCode) {

		List<Region> regions = new ArrayList<Region>();

		Query query = entityManager.createNativeQuery(" SELECT country, code, name, cities FROM regions where country=\"" + countryCode + "\"");
		List<Object[]> regionObjects = query.getResultList();

		for (Object[] object : regionObjects) {
			Region region = new Region();
			// TODO get id from another table
			region.setCountry(String.valueOf(object[0]));
			region.setRegionCode(String.valueOf(object[1]));
			region.setName(String.valueOf(object[2]));
			region.setCities(((Integer) object[3]).intValue());
			regions.add(region);
		}

		return regions;
	}

	@Cacheable("countries")
	public List<Country> getAllCountries() {

		List<Country> countries = new ArrayList<Country>();

		Query query = entityManager.createNativeQuery(" SELECT code, name FROM countrynames where locale=\"en_US\"");
		List<Object[]> countryObjects = query.getResultList();

		for (Object[] object : countryObjects) {
			Country country = new Country();
			// TODO get id from another table
			country.setCode(String.valueOf(object[0]));
			country.setName(String.valueOf(object[1]));
			countries.add(country);
		}

		return countries;
	}

}
