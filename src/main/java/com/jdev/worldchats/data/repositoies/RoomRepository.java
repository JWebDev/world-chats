/*
 * Created at 20.05.2015 by fe
 *
 * Copyright Klopotek & Partner GmbH. All Rights Reserved.
 *
 * Version history
 * ===============
 *
 * $Log: $
 */

package com.jdev.worldchats.data.repositoies;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.jdev.worldchats.data.domain.Room;

@Transactional(readOnly = true)
public interface RoomRepository extends JpaRepository<Room, Long>
{

}
