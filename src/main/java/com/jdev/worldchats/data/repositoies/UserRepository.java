/**
 * 
 */
package com.jdev.worldchats.data.repositoies;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.jdev.worldchats.data.domain.User;
import com.jdev.worldchats.data.enums.Gender;

/**
 * @author JDev
 *
 */
@Transactional(readOnly = true)
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("select u from USER u where u.nickname = :nickname and (u.isUserOnline=true or u.isPublicUser=false)")
	@Cacheable("users")
	User findByNicknameIsOnlineOrNotPublic(@Param("nickname") String nickname);

	@Cacheable("users")
	User findByNicknameAndIpAndGenderAndAge(String nickname, String ip, Gender gender, int age);
	
	@Cacheable("users")
	User findByEmailOrNickname(String email, String nickname);
	
	@Cacheable("users")
	User findByEmail(String email);
}
