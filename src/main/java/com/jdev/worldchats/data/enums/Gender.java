package com.jdev.worldchats.data.enums;

public enum Gender {
	MALE, FEMALE
}
