/*
 * Created at 20.05.2015 by fe
 *
 * Copyright Klopotek & Partner GmbH. All Rights Reserved.
 *
 * Version history
 * ===============
 *
 * $Log: $
 */

package com.jdev.worldchats.data.enums;

/**
 *
 * @author JDev
 */
public enum RoomType {

  PRIVATE("PRIVATE"), PUBLIC("PUBLIC"), CITY("CITY");

  private String value;


  private RoomType(String value)
  {
    this.value = value;
  }


  public String getValue()
  {
    return value;
  }


  public static RoomType fromString(String text)
  {
    if (text != null) {
      for (RoomType hType : RoomType.values()) {
        if (text.equalsIgnoreCase(hType.value)) {
          return hType;
        }
      }
    }
    return null;
  }
}
