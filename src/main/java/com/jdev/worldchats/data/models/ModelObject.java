/**
 * 
 */
package com.jdev.worldchats.data.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

import com.jdev.worldchats.commons.ErrorMessage;

/**
 * @author JDev
 *
 */
@Data
public class ModelObject implements Serializable {

	private List<ErrorMessage> errorMessages = new ArrayList<ErrorMessage>();

}