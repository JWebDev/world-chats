/*
 * Created at 20.05.2015 by fe
 *
 * Copyright Klopotek & Partner GmbH. All Rights Reserved.
 *
 * Version history
 * ===============
 *
 * $Log: $
 */

package com.jdev.worldchats.data.models;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

import com.jdev.worldchats.commons.ErrorMessage;
import com.jdev.worldchats.data.enums.Gender;

@Data
public class UserModel extends ModelObject implements Serializable {

//	public UserModel(List<ErrorMessage> errorMessages) {
//		super.setErrorMessages(errorMessages);
//	}

	private Long id;

	private String nickname;

	private String email;

	private Gender gender;

	private int age;

}
