/**
 * 
 */
package com.jdev.worldchats.data.models;

import lombok.Data;

/**
 * @author JDev
 *
 */
@Data
public class Country {
  
	private String code;
	private String name;
}
