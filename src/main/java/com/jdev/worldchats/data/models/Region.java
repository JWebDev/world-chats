/**
 * 
 */
package com.jdev.worldchats.data.models;

import lombok.Data;

/**
 * @author JDev
 *
 */
@Data
public class Region {
	
	private Integer id;
	private String country;
	private String regionCode;
	private String name;
	private Integer cities;
}
