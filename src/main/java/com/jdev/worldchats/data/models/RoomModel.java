/*
 * Created at 20.05.2015 by fe
 *
 * Copyright Klopotek & Partner GmbH. All Rights Reserved.
 * 
 * Version history
 * ===============
 * 
 * $Log: $
 */
 
package com.jdev.worldchats.data.models;

import java.util.List;

import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import com.jdev.worldchats.data.enums.RoomType;

/**
 * 
 * @author JDev
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RoomModel extends ModelObject {

  private static final long serialVersionUID = 1L;

  private Long              id;
  
  @Size(min = 5, max = 100, message = "roomModel.name.required")
  @NotBlank(message = "roomModel.name.required")
  private String            name;

  private RoomType          roomType;

  private Long              usersCount;

  private DateTime          created;
  
  private List<UserModel> users;

}
