package com.jdev.worldchats.data.models;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.jdev.worldchats.data.enums.Gender;

@Data
@EqualsAndHashCode(callSuper = false)
public class GoModel extends ModelObject
{

  private static final long serialVersionUID = 1L;

  private Long              id;

  @Size(min = 3, max = 20, message = "goModel.nickname.required")
  @NotBlank(message = "goModel.nickname.required")
  private String            nickname;

  private Gender            gender;

  @Range(min = 12, max = 100, message = "goModel.age.required")
  private int               age;
  
}
