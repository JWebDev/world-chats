/**
 * 
 */
package com.jdev.worldchats.data.models;

import lombok.Data;

/**
 * @author JDev
 *
 */
@Data
public class City {
	
	private Integer id;
	private String country;
	private String region;
	private String name;
}
