/**
 * 
 */
package com.jdev.worldchats.data.models;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.jdev.worldchats.data.enums.Gender;

/**
 * @author JDev
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SignUpModel extends ModelObject {

	private static final long serialVersionUID = 1L;

	private Long id;

	@Size(min = 3, max = 20, message = "signupModel.nickname.required")
	@NotBlank
	private String nickname;

	@Email(message = "signupModel.email.required")
	@Size(min = 5, max = 50, message = "signupModel.email.length")
	private String email;

	@NotBlank(message = "signupModel.password.required")
	private String password;

	private Gender gender;

	@Range(min = 12, max = 100, message = "signupModel.age.required")
	private int age;

}