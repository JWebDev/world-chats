package com.jdev.worldchats.data.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.jdev.worldchats.data.enums.Gender;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "USER")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "user_seq_gen")
	@SequenceGenerator(name = "user_seq_gen", sequenceName = "SEQ_WC_USER_ID")
	@Column(name = "ID")
	private Long id;

	@Column(nullable = false, name = "NICKNAME")
	private String nickname;

	@Column(nullable = false, name = "GENDER")
	private Gender gender;

	@Column(nullable = false, name = "IS_USER_ONLINE")
	private boolean isUserOnline;

	@Column(nullable = false, name = "IS_PUBLIC_USER")
	private boolean isPublicUser;

	@Column(nullable = false, name = "AGE")
	private int age;

	@Column(nullable = false, name = "IP")
	private String ip;

	@Column(nullable = true, name = "PASSWORD")
	private String password;

	@Column(nullable = true, name = "EMAIL")
	private String email;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(nullable = true, name = "REGISTERED")
	private DateTime registered;

	@Column(nullable = true, name = "ACTIVATION_KEY")
	private String activationKey;

	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = { javax.persistence.CascadeType.ALL })
	@JoinColumn(name = "USER_ID")
	@Column(nullable = false)
	private Set<LoginHistory> loginHistories;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(nullable = false, name = "CREATED")
	private DateTime created;

	@PrePersist
	void prePersist() {
		created = DateTime.now();
	}
}
