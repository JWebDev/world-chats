package com.jdev.worldchats.data.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.jdev.worldchats.data.enums.RoomType;

import lombok.Data;

@Data
@Entity(name = "ROOM")
public class Room implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "room_seq_gen")
	@SequenceGenerator(name = "room_seq_gen", sequenceName = "SEQ_WC_ROOM_ID")
	@Column(name = "ID")
	private Long id;

	@Column(nullable = false, name = "NAME")
	private String name;

	@Column(nullable = false, name = "ROOM_TYPE")
	private RoomType roomType;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(nullable = false, name = "CREATED")
	private DateTime created;

	// @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = {
	// javax.persistence.CascadeType.ALL })
	// @JoinColumn(name = "USER_ID")
	// @ManyToMany(cascade = { javax.persistence.CascadeType.ALL })
	// @JoinTable(name="ROOM_USER", joinColumns=@JoinColumn(name="ROOM_ID"),
	// inverseJoinColumns=@JoinColumn(name="USER_ID"))
	// @Column(nullable = false)

	// @ManyToMany(cascade = { javax.persistence.CascadeType.PERSIST })

	@ManyToMany
	@JoinTable(name = "ROOM_USER", joinColumns = { @JoinColumn(name = "ROOM_ID") }, inverseJoinColumns = { @JoinColumn(name = "USER_ID") })
	private Set<User> users;

	@PrePersist
	void prePersist() {
		created = DateTime.now();
	}

}
