package com.jdev.worldchats.data.domain;

import lombok.Data;

import org.joda.time.DateTime;

@Data
public class ChatMessage {

  private Long     id;

  private DateTime created;

  private String   text;

}
