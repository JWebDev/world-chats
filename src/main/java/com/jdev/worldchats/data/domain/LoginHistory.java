package com.jdev.worldchats.data.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;

import lombok.Data;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Data
@Entity(name="LOGIN_HISTORY")
public class LoginHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "log_history_seq_gen")
	@SequenceGenerator(name = "log_history_seq_gen", sequenceName = "SEQ_WC_LOG_HIST_ID")
	@Column(name = "ID")
	private Long id;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(nullable = false)
	private DateTime loginDate;
	
	@PrePersist
	void prePersist() {
		loginDate = DateTime.now();
	}

}
