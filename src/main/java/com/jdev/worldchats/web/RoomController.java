/*
 * Created at 20.05.2015 by fe
 *
 * Copyright Klopotek & Partner GmbH. All Rights Reserved.
 *
 * Version history
 * ===============
 *
 * $Log: $
 */

package com.jdev.worldchats.web;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;

import com.jdev.worldchats.data.domain.ChatMessage;
import com.jdev.worldchats.data.models.City;
import com.jdev.worldchats.data.models.Country;
import com.jdev.worldchats.data.models.Region;
import com.jdev.worldchats.data.models.RoomModel;
import com.jdev.worldchats.service.ChatService;
import com.jdev.worldchats.service.RoomService;
import com.jdev.worldchats.service.ValidatorService;

@Controller
@Path("/room")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RoomController
{


  @Inject
  private RoomService         roomService;

  @Inject
  private ChatService         chatService;

  @Inject
  private ValidatorService    validatorService;

  @Context
  private HttpServletResponse servletResponse;

  @Context
  private HttpServletRequest  servletRequest;


  @GET
  @Path("/private/{userId}")
  public RoomModel createPrivateRoom(@PathParam("userId") Long userId)
  {
    return roomService.createPrivateRoom(userId);
  }


  @GET
  @Path("/public/{userId}")
  public RoomModel createPublicRoom(@PathParam("userId") Long userId)
  {
    return roomService.createPublicRoom(userId);
  }


  @GET
  @Path("/city/{userId}")
  public RoomModel createCityRoom(@PathParam("userId") Long userId)
  {
    return roomService.createCityRoom(userId);
  }


  @GET
  @Path("/connect/{roomId}/user/{userId}")
  public RoomModel connectToRoom(@PathParam("roomId") Long roomId, @PathParam("userId") Long userId)
  {
    return roomService.connectToRoom(roomId, userId);
  }


  @GET
  @Path("/all")
  public List<RoomModel> getAllRooms()
  {
    return roomService.getAllRooms();
  }


  @GET
  @Path("/countries")
  public List<Country> countries()
  {
    return roomService.getAllCountries();
  }


  @GET
  @Path("/country/{countryCode}/regions")
  public List<Region> regions(@PathParam("countryCode") String countryCode)
  {
    return roomService.getAllRegions(countryCode);
  }


  @GET
  @Path("/country/{countryCode}/region/{regionCode}")
  public List<City> cities(@PathParam("countryCode") String countryCode, @PathParam("regionCode") String regionCode)
  {
    return roomService.getAllCities(countryCode, regionCode);
  }


  @POST
  @Path("/{roomId}/chat/{chatId}")
  public void chat(@PathParam("roomId") String roomId, @PathParam("chatId") String chatId, ChatMessage message)
  {
    chatService.chat(roomId, chatId, message);
  }

}
