package com.jdev.worldchats.web;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;

import com.jdev.worldchats.commons.PublicContext;
import com.jdev.worldchats.commons.WorldChatsConstant;
import com.jdev.worldchats.commons.WorldChatsException;
import com.jdev.worldchats.data.domain.User;
import com.jdev.worldchats.data.models.GoModel;
import com.jdev.worldchats.data.models.SignUpModel;
import com.jdev.worldchats.data.models.UserModel;
import com.jdev.worldchats.service.ValidatorService;
import com.jdev.worldchats.service.WorldChatsService;

@Controller
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WorldChatsController {

	@Inject
	private WorldChatsService worldChatsService;

	@Inject
	private ValidatorService validatorService;

	@Context
	private HttpServletResponse servletResponse;

	@Context
	private HttpServletRequest servletRequest;
	
//	@Context
//	private HttpSession httpSession;

	@GET
	public String home() {
		return "WORLDCHATS_SESSION_ID";
	}

	/**
	 * Validates the model, if model has errors, returns the model with errors
	 * object. If the validation is successful, the model is sent to the
	 * service.
	 * 
	 * @param goModel
	 *            - model for the easy registration from the client
	 * @return user model
	 * @throws WorldChatsException
	 */
	@POST
	@Path("/go")
	public UserModel goToChats(GoModel goModel) throws WorldChatsException {

		validatorService.validateModel(goModel);
		UserModel userModel = new UserModel();

		if (goModel.getErrorMessages().isEmpty()) {
			servletResponse.setStatus(HttpServletResponse.SC_OK);
			servletResponse.addHeader(WorldChatsConstant.SESSION_ID_HTTP_HEADER, PublicContext.getSessionId());
			servletResponse.getHeaders("Cookie").clear();
//			Cookie[] cookies = servletRequest.getCookies();
//			if(cookies!=null)
//			for (int i = 0; i < cookies.length; i++) {
//			 cookies[i].setMaxAge(0);
//			}
			userModel = worldChatsService.savePublicUser(goModel);
		} else {
			servletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			userModel.setErrorMessages(goModel.getErrorMessages());
		}
		return userModel;
	}

	/**
	 * Validates the model, if model has errors, returns the model with errors
	 * object. If the validation is successful, the model is sent to the
	 * service.
	 * 
	 * @param signupModel
	 *            - model for the full registration from the client
	 * @return UserModel
	 * @throws WorldChatsException
	 */
	@POST
	@Path("/signup")
	public UserModel signup(SignUpModel signupModel) throws WorldChatsException {

		validatorService.validateModel(signupModel);
		UserModel userModel = new UserModel();

		if (signupModel.getErrorMessages().isEmpty()) {
			servletResponse.setStatus(HttpServletResponse.SC_OK);
			userModel = worldChatsService.signup(signupModel);
			servletResponse.addHeader(WorldChatsConstant.SESSION_ID_HTTP_HEADER, PublicContext.getSessionId());
		} else {
			servletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			userModel.setErrorMessages(signupModel.getErrorMessages());
		}
		return userModel;
	}

	@POST
	@Path("/signin")
	@Consumes("multipart/form-data")
	public UserModel signin(@FormParam(value = "id") String id, @FormParam("password") String password) throws WorldChatsException {

		servletResponse.setStatus(HttpServletResponse.SC_OK);

		return worldChatsService.signin(id, password);
	}

	@GET
	@Path("/logout")
	public String logout() {
//		httpSession.invalidate();
		servletRequest.getSession().invalidate();
		// TODO session invalidate
		return "logged out";
	}

	@GET
	@Path("/getAll")
	public List<User> getAll() {
		return worldChatsService.findAll();
	}

}
