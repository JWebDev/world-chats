package com.jdev.worldchats.commons;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * the field having validation errors
	 */
	private String field;
	
	/**
	 * the message to know what is wrong
	 */
	private String message;
}
