/**
 * 
 */
package com.jdev.worldchats.commons;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * A base exception handler for all another errors in microservice.
 *
 * @author JDev
 */
public class WorldChatsExceptionMapper implements ExceptionMapper<WorldChatsException> {
	
	@Override
	public Response toResponse(WorldChatsException exception) {

		ResponseBuilder responseBuilder;
		// List<MediaType> acceptableMediaTypes =
		// _headers.getAcceptableMediaTypes();

//		if ((exception instanceof ServerErrorException) || (exception instanceof ClientErrorException)) {
//			WebApplicationException webAppexception = (WebApplicationException) exception;
//			responseBuilder = Response.status(webAppexception.getResponse().getStatus());
//			responseBuilder.entity(exception.getMessage());
//		} else {
			responseBuilder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
			responseBuilder.entity(exception);
//		}
		// TODO acceptableMediaTypes.size() immer */*
		// responseBuilder.type(acceptableMediaTypes.size() == 0 ?
		// MediaType.valueOf(MediaType.APPLICATION_JSON) :
		// acceptableMediaTypes.get(0));
		responseBuilder.type(MediaType.APPLICATION_JSON);
		responseBuilder.header(WorldChatsConstant.EXCEPTION_HTTP_HEADER, exception.getMessage() == null ? "Internal Server Error" : exception.getMessage());

		return responseBuilder.build();

	}
}
