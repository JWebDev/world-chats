/**
 * 
 */
package com.jdev.worldchats.commons;

import org.joda.time.DateTime;

/**
 * @author JDev
 *
 */
public class PublicContext {

	private static InheritableThreadLocal<Long> userId = new InheritableThreadLocal<Long>();
	private static InheritableThreadLocal<String> sessionId = new InheritableThreadLocal<String>();
	private static InheritableThreadLocal<DateTime> createdAt = new InheritableThreadLocal<DateTime>();
	private static InheritableThreadLocal<String> userIpAddress = new InheritableThreadLocal<String>();

	// private static InheritableThreadLocal<DateTime> _validUntil = new
	// InheritableThreadLocal<DateTime>();
	// private static InheritableThreadLocal<Boolean> _isValid = new
	// InheritableThreadLocal<Boolean>();


	public static String getUserIpAddress() {
		return userIpAddress.get();
	}

	public static void setUserIpAddress(String ip) {
		userIpAddress.set(ip);
	}

	public static Long getUserId() {
		return userId.get();
	}

	public static void setUserId(Long id) {
		userId.set(id);
	}

	public static String getSessionId() {
		return sessionId.get();
	}

	public static void setSessionId(String session) {
		sessionId.set(session);
	}

	public static DateTime getCreatedAt() {
		return createdAt.get();
	}

	public static void setCreatedAt(DateTime created) {
		createdAt.set(created);
	}

}
