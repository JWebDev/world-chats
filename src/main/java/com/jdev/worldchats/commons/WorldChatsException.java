/**
 * 
 */
package com.jdev.worldchats.commons;

import lombok.Getter;

/**
 * @author JDev
 *
 */
public class WorldChatsException extends Exception{
	

	private static final long serialVersionUID = 1L;

	@Getter
	private final String messageKey;
	
	@Getter
	private final String message;

	public WorldChatsException() {
		this.messageKey = null;
		this.message = null;
	}

	/**
	 * Constructs a new ServiceException exception with the specified detail
	 * message.
	 * 
	 * @param message
	 */
	public WorldChatsException(String message) {
		this.messageKey = null;
		this.message = message;
	}

	/**
	 * Constructs a new ServiceException exception with the specified detail
	 * message.
	 * 
	 * @param message
	 * @param messageKey
	 */
	public WorldChatsException(String message, String messageKey) {
		this.messageKey = messageKey;
		this.message = message;
	}

}
