/**
 * 
 */
package com.jdev.worldchats.commons;

/**
 * @author JDev
 *
 */
public class WorldChatsConstant {
	public static String SESSION_ID_HTTP_HEADER = "JSESSIONID";
	// public static String SESSION_ID_CONTEXT = "KAS_SESSION_ID";
	public static String EXCEPTION_HTTP_HEADER = "Exception";
}
