/**
 * 
 */
package com.jdev.worldchats.commons;

import java.io.Serializable;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * @author JDev
 *
 */
@Data
@RequiredArgsConstructor
public class HttpErrorMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String statusCode;
	
	private final int value;
	
	private final String message;

}